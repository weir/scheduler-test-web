package com.aurora.scheduler.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.aurora.scheduler.service.IScheduleJobService;

@Controller
@RequestMapping("system/scheduleJob")
public class ScheduleJobController {

	@Autowired
	private IScheduleJobService scheduleJobService;
	
	/*@RequestMapping("/list")
	public String list(Integer page, String query, Model model) {
		model.addAttribute("page", page);
		model.addAttribute("query", query);
		PageView<ScheduleJob> pageView = new PageView<ScheduleJob>(8, page == null || page < 1 ? 1 : page);
		if("true".equals(query)) {
			StringBuffer jpql = new StringBuffer("");
			List<Object> params = new ArrayList<Object>();
			pageView.setQueryResult(scheduleJobService.getScrollList(pageView.getFirstResult(), pageView.getMaxresult(), jpql.toString(), params.toArray()));
		} else {
			pageView.setQueryResult(scheduleJobService.getScrollList(pageView.getFirstResult(), pageView.getMaxresult()));
		}
		model.addAttribute("pageView", pageView);
		model.addAttribute("executingJobs", scheduleJobService.queryExecutingJobList());
		return "jsp/system/scheduleJob/scheduleJob_list";
	}
	@RequestMapping("/initjob")
	@ResponseBody
	public Json initjob() {
		scheduleJobService.init();
		return json("初始化成功");
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(ScheduleJobVo scheduleJobVo) {
		ScheduleJob scheduleJob = new ScheduleJob();
		BeanUtils.copyProperties(scheduleJobVo, scheduleJob);
		scheduleJob.setSch_create(new Date());
		try {
			scheduleJobService.save(scheduleJob);
			return json(true, "定时任务添加成功");
		} catch (Exception e) {
			e.printStackTrace();
			return json("定时任务添加失败");
		}
	}
	@RequestMapping("/editip")
	public String editip(Integer id,Model model) {
		model.addAttribute("scheduleJob", scheduleJobService.find(id));
		return "jsp/system/scheduleJob/scheduleJob_edit";
	}
	@RequestMapping("/edit")
	@ResponseBody
	public Json edit(ScheduleJobVo scheduleJobVo) {
		if (ValidateUtil.isEmpty(scheduleJobVo.getSch_id())) {
			return json("定时任务ID没有获取到");
		}
		ScheduleJob scheduleJob = scheduleJobService.find(scheduleJobVo.getSch_id());
		BeanUtils.copyProperties(scheduleJobVo, scheduleJob);
		try {
			scheduleJobService.update(scheduleJob);
			return json(true, "定时任务修改成功");
		} catch (Exception e) {
			e.printStackTrace();
			return json("定时任务修改失败");
		}
	}*/
}