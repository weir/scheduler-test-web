package com.aurora.scheduler.controller;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aurora.scheduler.service.IScheduleJobService;

/**
 * 定时任务初始化
 */
@Component
public class ScheduleJobInit {

    /** 定时任务service */
    @Autowired
    private IScheduleJobService  scheduleJobService;

    /**
     * 项目启动时初始化
     */
    @PostConstruct
    public void init() {
        scheduleJobService.init();
    }

}