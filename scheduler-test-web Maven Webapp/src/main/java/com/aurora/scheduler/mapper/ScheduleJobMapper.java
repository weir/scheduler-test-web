package com.aurora.scheduler.mapper;

import com.aurora.scheduler.entity.ScheduleJob;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author weir
 * @since 2017-07-10
 */
public interface ScheduleJobMapper extends BaseMapper<ScheduleJob> {

}