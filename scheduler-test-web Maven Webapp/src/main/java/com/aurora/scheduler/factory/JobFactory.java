package com.aurora.scheduler.factory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.aurora.scheduler.entity.ScheduleJob;

/**
 * 任务工厂类,保证多个任务间不会同时执行.所以在多任务执行时最好加上
 */
@DisallowConcurrentExecution
public class JobFactory implements Job {

    public void execute(JobExecutionContext context) throws JobExecutionException {

        ScheduleJob scheduleJob = (ScheduleJob) context.getMergedJobDataMap().get("jobParam");

        System.out.println("jobName:" + scheduleJob.getName() + "  " + scheduleJob);

    }
}