package com.aurora.scheduler.factory;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.aurora.scheduler.entity.ScheduleJob;

/**
 * 任务工厂类,并发
 */
public class JobSyncFactory implements Job {

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        JobDataMap mergedJobDataMap = jobExecutionContext.getMergedJobDataMap();
        ScheduleJob scheduleJob = (ScheduleJob) mergedJobDataMap.get("jobParam");

        System.out.println("jobName:" + scheduleJob.getName() + "  " + scheduleJob);

    }
}