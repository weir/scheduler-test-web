package com.aurora.scheduler.service;

import java.util.List;

import com.aurora.scheduler.entity.ScheduleJob;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weir
 * @since 2017-07-10
 */
public interface IScheduleJobService extends IService<ScheduleJob> {
	void init();
	List<ScheduleJob> queryExecutingJobList();
}
