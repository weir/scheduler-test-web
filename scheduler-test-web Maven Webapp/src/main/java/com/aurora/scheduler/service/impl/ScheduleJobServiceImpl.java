package com.aurora.scheduler.service.impl;

import com.aurora.common.util.BaseUtil;
import com.aurora.scheduler.entity.ScheduleJob;
import com.aurora.scheduler.mapper.ScheduleJobMapper;
import com.aurora.scheduler.service.IScheduleJobService;
import com.aurora.scheduler.util.ScheduleUtils;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weir
 * @since 2017-07-10
 */
@Service
public class ScheduleJobServiceImpl extends ServiceImpl<ScheduleJobMapper, ScheduleJob> implements IScheduleJobService {
	@Autowired
	private Scheduler scheduler;

	public void init() {
		List<ScheduleJob> scheduleJobs = selectList(null);
		for (ScheduleJob scheduleJob : scheduleJobs) {
			CronTrigger cronTrigger;
			try {
				cronTrigger = ScheduleUtils.getCronTrigger(scheduler,
						scheduleJob.getName(), scheduleJob.getGroup());
				if (cronTrigger!=null) {
//					ScheduleUtils.deleteScheduleJob(scheduler, scheduleJob.getName(), scheduleJob.getGroup());
					// 已存在，那么更新相应的定时设置
					ScheduleUtils.updateScheduleJob(scheduler, scheduleJob);
				}else {
					ScheduleUtils.createScheduleJob(scheduler, scheduleJob);
				}
				
				/*if (scheduleJob.getIsSync()) {
					if (BaseUtil.isEmpty(scheduleJob.getClassNameSync())) {
						continue;
					}
				}else {
					if (BaseUtil.isEmpty(scheduleJob.getClassName())) {
						continue;
					}
				}*/
				
				
				/*// 不存在，创建一个
				if (cronTrigger == null) {
					
				} else {
					// 已存在，那么更新相应的定时设置
					ScheduleUtils.updateScheduleJob(scheduler, scheduleJob);
				}*/
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
		}
	}

	public List<ScheduleJob> queryExecutingJobList() {
		try {
			List<JobExecutionContext> executingJobs = executingJobs = scheduler.getCurrentlyExecutingJobs();;
			/*while (true) {
				executingJobs = scheduler.getCurrentlyExecutingJobs();
				if (executingJobs.size()>0) {
					break;
				}
			}*/
			
			List<ScheduleJob> jobList = new ArrayList<ScheduleJob>(
					executingJobs.size());
			for (JobExecutionContext executingJob : executingJobs) {
				ScheduleJob job = new ScheduleJob();
				JobDetail jobDetail = executingJob.getJobDetail();
				JobKey jobKey = jobDetail.getKey();
				Trigger trigger = executingJob.getTrigger();
				job.setName(jobKey.getName());
				job.setGroup(jobKey.getGroup());
				job.setJobTrigger(trigger.getKey().getName());
				Trigger.TriggerState triggerState = scheduler
						.getTriggerState(trigger.getKey());
				job.setStatus(triggerState.name());
				if (trigger instanceof CronTrigger) {
					CronTrigger cronTrigger = (CronTrigger) trigger;
					String cronExpression = cronTrigger.getCronExpression();
					job.setCronExpression(cronExpression);
				}
				jobList.add(job);
			}
			return jobList;
		} catch (SchedulerException e) {
			e.printStackTrace();
			return null;
		}

	}
}
