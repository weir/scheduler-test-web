/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : aurora_erp

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-07-21 09:33:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(100) DEFAULT NULL COMMENT '任务名称',
  `alias_name` varchar(50) DEFAULT NULL COMMENT '任务别名',
  `group` varchar(50) DEFAULT NULL COMMENT '任务分组',
  `job_trigger` varchar(100) DEFAULT NULL COMMENT '触发器',
  `status` varchar(10) DEFAULT NULL COMMENT '任务状态',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT '任务运行时间表达式',
  `isSync` tinyint(1) DEFAULT '0' COMMENT '是否异步',
  `description` varchar(200) DEFAULT NULL COMMENT '任务描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `class_name` varchar(100) DEFAULT NULL COMMENT '要执行的任务类',
  `class_name_sync` varchar(100) DEFAULT '' COMMENT '要执行的任务类(异步)',
  `method_name` varchar(20) DEFAULT NULL COMMENT '方法名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='定时任务持久化';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', '测试', null, 'weir', 'ww', '', '0/5 * * * * ?', '0', null, null, null, 'com.aurora.scheduler.factory.JobFactory', 'com.aurora.scheduler.factory.JobSyncFactory', 'tt');
