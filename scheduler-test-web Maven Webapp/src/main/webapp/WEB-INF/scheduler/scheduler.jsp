<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="aurorapt" uri="http://java.aurora.com/aurora/permission"%>
<!DOCTYPE html>
<html>
<head>
<title>定时任务管理</title>
<meta name="content-type" content="text/html; charset=UTF-8">
<jsp:include page="/pub.jsp"></jsp:include>
<script type="text/javascript">
var userDataGrid;
$(function() {
	userDataGrid = $('#user_datagrid').datagrid({
		url : '${pageContext.request.contextPath}/user/list',
		fit : true,
		fitColumns : true,
		border : false,
		pagination : true,
		idField : 'id',
		pagePosition : 'both',
		checkOnSelect:true,
		selectOnCheck:true,
		columns : [ [ {
			field : 'id',
			title : '编号',
			width : 100,
			checkbox : true
		}, {
			field : 'no',
			title : '编号',
			width : 50
		}, {
			field : 'name',
			title : '用户名',
			width : 70
		}, {
			field : 'email',
			title : '邮箱',
			width : 100
		}, {
			field : 'mphone',
			title : '手机号',
			width : 100
		}, {
			field : 'realname',
			title : '姓名',
			width : 40
		}, {
			field : 'sex',
			title : '性别',
			width : 20
		}, {
			field : 'birthday',
			title : '生日',
			width : 50,
			formatter: function(value,row,index){
				if(row.birthday!=undefined){
					return getFormatDateByLong(row.birthday,"yyyy-MM-dd");
				}
			}
		}, {
			field : 'weixin',
			title : '微信',
			width : 50
		}, {
			field : 'department',
			title : '部门',
			width : 80
		}, {
			field : 'region',
			title : '地区',
			width : 100
		}, {
			field : 'postName',
			title : '职务',
			width : 100
		}] ],
		onRowContextMenu:function(e, rowIndex, rowData){
			e.preventDefault();
			$(this).datagrid('clearSelections');
			$(this).datagrid('clearChecked');
			$(this).datagrid('selectRow',rowIndex);
			$('#user_menu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});
		},toolbar:'#tb'
	});
	
});

function userAdd() {
	var dialog = parent.modalDialog({
		title : '用户添加',
		width : 700,
		height : 500,
		url : '${pageContext.request.contextPath}/user/addUI',
		buttons : [ {
			text : '添加',
			handler : function() {
				dialog.find('iframe').get(0).contentWindow.useradd_submitForm(dialog, userDataGrid, parent.$);
			}
		} ]
	});
}

function userEdit(){
	var rows = userDataGrid.datagrid('getChecked');
	if(rows.length==1){
		var dialog = parent.modalDialog({
			title : '用户修改',
			width : 700,
			height : 500,
			url : '${pageContext.request.contextPath}/user/editUI/'+rows[0].id,
			buttons : [ {
				text : '添加',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.useredit_submitForm(dialog, userDataGrid, parent.$);
				}
			} ]
		});
	}else{
		parent.$.messager.alert('提示','请选择一条记录进行修改');
	}
}
function userDelete(){
	var rows = userDataGrid.datagrid('getChecked');
	if(rows.length==1){
		$.messager.confirm('确认','您确认想要删除记录吗？',function(r){
		    if (r){
				$.post('${pageContext.request.contextPath}/user/delete', {id:rows[0].id}, function(j) {
					if (j.success) {
						userDataGrid.datagrid('reload');
					}
					userDataGrid.datagrid('uncheckAll');
					$.messager.show({
						title : '提示',
						msg : j.msg,
						timeout : 5000,
						showType : 'slide'
					});
				}, 'json').error(function() {
					$.messager.show({
						title : '提示',
						msg : '你的权限不够',
						timeout : 5000,
						showType : 'slide'
					});
				});
		    }    
		});
	}else{
		parent.$.messager.alert('提示','请选择一条记录进行删除');
	}
}

function userRole(){
	var rows = userDataGrid.datagrid('getChecked');
	if(rows.length==1){
		var dialog = parent.modalDialog({
			title : '用户角色授权',
			width : 350,
			height : 300,
			url : '${pageContext.request.contextPath}/user/roleUI/'+rows[0].id,
			buttons : [ {
				text : '授权',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.userrole_submitForm(dialog, userDataGrid, parent.$);
				}
			} ]
		});
	}else{
		parent.$.messager.alert('提示','请选择一条记录进行授权');
	}
}

function user_searchFun() {
	$('#user_datagrid').datagrid('load', serializeObject($('#user_searchForm')));
}
function user_clearFun() {
	$('#user_searchForm input').val('');
	//$('#admin_yhgl_datagrid').datagrid('load', {});
}
</script>
</head>

<body>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false,title:'用户列表'" style="overflow: hidden;">
		<table id="user_datagrid"></table>
	</div>
</div>

<div id="tb" style="padding:2px 5px;">
<form id="user_searchForm">
<aurorapt:permission privilege="user_addUI">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-add" onclick="userAdd();">添加</a></aurorapt:permission>
<aurorapt:permission privilege="user_editUI">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-pencil" onclick="userEdit();">修改</a></aurorapt:permission>
<aurorapt:permission privilege="user_roleUI">
<a href="javascript:void(0);" class="easyui-linkbutton" iconCls="ext-icon-pencil" onclick="userRole();">授权</a></aurorapt:permission>
姓名：<input class="easyui-textbox" name="realname"/>
手机：<input class="easyui-textbox" name="mphone"/>
        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="user_searchFun();">查询</a>
        <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-clear" onclick="user_clearFun();">清空</a>
        </form>
    </div>

<div id="user_menu" class="easyui-menu" style="width: 120px;display: none;">
<aurorapt:permission privilege="user_addUI">
<div onclick="userAdd()" iconCls="icon-add">增加</div></aurorapt:permission>
<aurorapt:permission privilege="user_editUI">
<div onclick="userEdit()" iconCls="icon-edit">编辑</div></aurorapt:permission>
<aurorapt:permission privilege="user_roleUI">
<div onclick="userRole()" iconCls="icon-add">角色授权</div></aurorapt:permission>
</div>
</body>
</html>