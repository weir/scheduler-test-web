package com.aurora.common.util;

import java.net.URI;
import java.net.URISyntaxException;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.ServiceHelper;
import cn.jiguang.common.connection.NettyHttpClient;
import cn.jiguang.common.resp.ResponseWrapper;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;
import io.netty.handler.codec.http.HttpMethod;

public class TestMessagePush {

    private final static String APP_KEY = "a8d4f65fcb9e9e49c371943a";

    private final static String MASTER_SECRET = "1fc91f4a93ff4d7c0f069681";

    /**
     * 向所有设备的特定用户发送通知
     * @param alert 的内容 message
     * @param 别名 alias 约定为userid
     * @return
     */
    private static PushPayload buildPushObject_all_alert_withAlias(String message, String userid) {
    	System.out.println("buildPushObject_all_alert_withAlias"+"别名为："+userid);
        return PushPayload.newBuilder()
        		.setPlatform(Platform.all())
        		//.setAudience(Audience.all())
        		.setAudience(Audience.alias(userid))
        		.setNotification(Notification.newBuilder().setAlert(message).build())
        		.setOptions(Options.newBuilder()
                        //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                        //.setApnsProduction(true)
                        //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                        .setSendno(1)
                        //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
                        .setTimeToLive(864000)//10天
                        .build())
    			.build();
    }
    
    /**
     * 向android设备的特定用户发送通知
     * @param alert 的内容 message
     * @param 别名 alias 约定为userid
     * @return
     */
    private static PushPayload buildPushObject_android_alert_withAlias(String message, String userid) {
    	System.out.println("buildPushObject_android_alert_withAlias"+"别名为："+userid);
    	return PushPayload.newBuilder()
    			.setPlatform(Platform.android())
    			//.setAudience(Audience.all())
    			.setAudience(Audience.alias(userid))
    			.setNotification(Notification.newBuilder().setAlert(message).build())
    			.setOptions(Options.newBuilder()
    					//此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
    					//.setApnsProduction(true)
    					//此字段是给开发者自己给推送编号，方便推送者分辨推送记录
    					.setSendno(1)
    					//此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
    					.setTimeToLive(864000)//10天
    					.build())
    			.build();
    }
    
    /**
     * 向ios设备的特定用户发送通知
     * @param alert 的内容 message
     * @param 别名 alias 约定为userid
     * @return
     */
    private static PushPayload buildPushObject_ios_alert_withAlias(String message, String userid) {
    	System.out.println("buildPushObject_ios_alert_withAlias"+"别名为："+userid);
    	return PushPayload.newBuilder()
    			.setPlatform(Platform.ios())
    			//.setAudience(Audience.all())
    			.setAudience(Audience.alias(userid))
    			.setNotification(Notification.newBuilder().setAlert(message).build())
    			.setOptions(Options.newBuilder()
    					//此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
    					//.setApnsProduction(true)
    					//此字段是给开发者自己给推送编号，方便推送者分辨推送记录
    					.setSendno(1)
    					//此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
    					.setTimeToLive(864000)//10天
    					.build())
    			.build();
    }
    
    /**
     * 向所有设备的用户发送通知
     * @param message
     * @return
     */
    private static PushPayload buildPushObject_all_alert(String message) {
    	System.out.println("buildPushObject_all_alert");
    	return PushPayload.newBuilder()
    			.setPlatform(Platform.all())
    			.setAudience(Audience.all())
    			.setNotification(Notification.newBuilder().setAlert(message).build())
    			.setOptions(Options.newBuilder()
    					//此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
    					//.setApnsProduction(true)
    					//此字段是给开发者自己给推送编号，方便推送者分辨推送记录
    					.setSendno(1)
    					//此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
    					.setTimeToLive(864000)//10天
    					.build())
    			.build();
    }
    
    
    /**
     * 向Android设备的用户发送通知
     * @param message
     * @return
     */
    private static PushPayload buildPushObject_android_alert(String message) {
    	System.out.println("buildPushObject_android_alert");
    	return PushPayload.newBuilder()
    			.setPlatform(Platform.android())
    			.setAudience(Audience.all())
    			.setNotification(Notification.newBuilder().setAlert(message).build())
    			.setOptions(Options.newBuilder()
    					//此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
    					//.setApnsProduction(true)
    					//此字段是给开发者自己给推送编号，方便推送者分辨推送记录
    					.setSendno(1)
    					//此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
    					.setTimeToLive(864000)//10天
    					.build())
    			.build();
    }
    
    /**
     * 向IOS设备的用户发送通知
     * @param message
     * @return
     */
    private static PushPayload buildPushObject_ios_alert(String message) {
    	System.out.println("buildPushObject_ios_alert");
    	return PushPayload.newBuilder()
    			.setPlatform(Platform.ios())
    			.setAudience(Audience.all())
    			.setNotification(Notification.newBuilder().setAlert(message).build())
    			.setOptions(Options.newBuilder()
    					//此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
    					//.setApnsProduction(true)
    					//此字段是给开发者自己给推送编号，方便推送者分辨推送记录
    					.setSendno(1)
    					//此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
    					.setTimeToLive(864000)//10天
    					.build())
    			.build();
    }
    
    /**
     * 向所有设备异步发通知
     * @param message
     */
    public static void sendAlertAll(PushPayload payload) {
    	ClientConfig clientConfig = ClientConfig.getInstance();
        String host = (String) clientConfig.get(ClientConfig.PUSH_HOST_NAME);
        final NettyHttpClient client = new NettyHttpClient(ServiceHelper.getBasicAuthorization(APP_KEY, MASTER_SECRET),
                null, clientConfig);
        try {
            URI uri = new URI(host + clientConfig.get(ClientConfig.PUSH_PATH));
            //PushPayload payload = buildPushObject_all_alert(message);
            client.sendRequest(HttpMethod.POST, payload.toString(), uri, new NettyHttpClient.BaseCallback() {
                @Override
                public void onSucceed(ResponseWrapper responseWrapper) {
//                    LOG.info("Got result: " + responseWrapper.responseContent);
                }
            });
            client.close();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args){
//    	TestMessagePush.sendAlertAll(TestMessagePush.buildPushObject_all_alert("all发送广播"));
//    	TestMessagePush.sendAlertAll(TestMessagePush.buildPushObject_android_alert("android发送广播"));
//    	TestMessagePush.sendAlertAll(TestMessagePush.buildPushObject_ios_alert("ios发送广播"));
//    	TestMessagePush.sendAlertAll(TestMessagePush.buildPushObject_all_alert_withAlias("all发送通知","815c2802cff34d7292204d7af532aa45"));
//    	TestMessagePush.sendAlertAll(TestMessagePush.buildPushObject_android_alert_withAlias("android发送通知","815c2802cff34d7292204d7af532aa45"));
//    	TestMessagePush.sendAlertAll(TestMessagePush.buildPushObject_ios_alert_withAlias("ios发送通知","815c2802cff34d7292204d7af532aa45"));
//    	
    	System.out.println("10000.jpg".substring(0,"10000.jpg".lastIndexOf(".")));
    }
}
