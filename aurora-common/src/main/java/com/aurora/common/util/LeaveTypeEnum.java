package com.aurora.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * 
* @ClassName: LeaveEnum 
* @Description: 请假类别
* @author weir 634623907@qq.com 
* @date 2016年12月29日 上午11:09:26 
*
 */
public enum LeaveTypeEnum {

	personal_leave("事假"),
	sick_leave("病假"),
	marital_leave("婚假"),
	funeral_leave("丧假"),
	public_leave("公假"),
	maternity_leave("产假"),
	care_leave("护理假"),
	injury_leave("工伤假"),
	other_leave("其他");
	
	/** 描述 */
	private String desc;

	private LeaveTypeEnum(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static Map<String, Map<String, Object>> toMap() {
		LeaveTypeEnum[] ary = LeaveTypeEnum.values();
		Map<String, Map<String, Object>> enumMap = new HashMap<String, Map<String, Object>>();
		for (int num = 0; num < ary.length; num++) {
			Map<String, Object> map = new HashMap<String, Object>();
			String key = ary[num].name();
			map.put("desc", ary[num].getDesc());
			enumMap.put(key, map);
		}
		return enumMap;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List toList() {
		LeaveTypeEnum[] ary = LeaveTypeEnum.values();
		List list = new ArrayList();
		for (int i = 0; i < ary.length; i++) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("desc", ary[i].getDesc());
			map.put("name", ary[i].name());
			list.add(map);
		}
		return list;
	}

	public static LeaveTypeEnum getEnum(String name) {
		LeaveTypeEnum[] arry = LeaveTypeEnum.values();
		for (int i = 0; i < arry.length; i++) {
			if (arry[i].name().equalsIgnoreCase(name)) {
				return arry[i];
			}
		}
		return null;
	}

	/**
	 * 取枚举的json字符串
	 *
	 * @return
	 */
	public static String getJsonStr() {
		LeaveTypeEnum[] enums = LeaveTypeEnum.values();
		StringBuffer jsonStr = new StringBuffer("[");
		for (LeaveTypeEnum senum : enums) {
			if (!"[".equals(jsonStr.toString())) {
				jsonStr.append(",");
			}
			jsonStr.append("{\"id\":\"").append(senum).append("\",\"desc\":\"").append(senum.getDesc()).append("\"}");
		}
		jsonStr.append("]");
		return jsonStr.toString();
	}
	
	public static void main(String[] args) {
		System.out.println(LeaveTypeEnum.getJsonStr());
	}
}
