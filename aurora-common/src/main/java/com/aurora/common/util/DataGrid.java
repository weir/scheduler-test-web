package com.aurora.common.util;

import java.util.ArrayList;
import java.util.List;
/**
 * 
* @ClassName: DataGrid 
* @Description: easyui数据封装
* @author weir
* @date 2016年11月10日 下午2:32:25 
* 
* @param <T>
 */
public class DataGrid<T> {

	private int total = 0;
	private List<T> rows = new ArrayList<T>();

	public DataGrid() {
		super();
	}

	public DataGrid(int total, List<T> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

}
