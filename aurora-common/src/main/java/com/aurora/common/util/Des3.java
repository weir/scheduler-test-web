package com.aurora.common.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;


/**
 * android 和 java 和 ios同一加密解密算法
 * @author JackJin
 *
 */
public class Des3 {
	// 密钥  
    private final static String secretKey = "liuyunqiang@lx100$#365#$";  
    // 向量  
    private final static String iv = "01234567";  
    // 加解密统一使用的编码方式  
    private final static String encoding = "utf-8";  
  
    /** 
     * 3DES加密 
     *  
     * @param plainText 普通文本 
     * @return 
     * @throws Exception  
     */  
    public static String encode(String plainText) throws Exception {  
        Key deskey = null;  
        DESedeKeySpec spec = new DESedeKeySpec(secretKey.getBytes());  
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");  
        deskey = keyfactory.generateSecret(spec);  
  
        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");  
        IvParameterSpec ips = new IvParameterSpec(iv.getBytes());  
        cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);  
        byte[] encryptData = cipher.doFinal(plainText.getBytes(encoding));  
        return Base64.encode(encryptData);  
    }  
  
    /** 
     * 3DES解密 
     *  
     * @param encryptText 加密文本 
     * @return 
     * @throws Exception 
     */  
    public static String decode(String encryptText) throws Exception {  
        Key deskey = null;  
        DESedeKeySpec spec = new DESedeKeySpec(secretKey.getBytes());  
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");  
        deskey = keyfactory.generateSecret(spec);  
        Cipher cipher = Cipher.getInstance("desede/CBC/PKCS5Padding");  
        IvParameterSpec ips = new IvParameterSpec(iv.getBytes());  
        cipher.init(Cipher.DECRYPT_MODE, deskey, ips);  
  
        byte[] decryptData = cipher.doFinal(Base64.decode(encryptText));  
  
        return new String(decryptData, encoding);  
    }
    
    public static void  main(String[] args) throws Exception {
    	
		String encodeStr = Des3.encode("");
		System.out.println("encodeStr==>"+encodeStr);
		String decodeStr = Des3.decode(encodeStr);
		System.out.println("decodeStr==>"+decodeStr);
		String requestUri = "http://oa.rsaurora.com.cn/traffic/index";
		String contextPath = "http://oa.rsaurora.com.cn";
		String url = requestUri.substring(contextPath.length());
    	if(url != "" && url.contains("traffic")){
    		System.out.println(contextPath+"/traffic/login.jsp");
    	}else {
    		System.out.println(contextPath+"/login.jsp");
		}
    	
    	String token = "1;";
    	if (token.contains(";")) {
    		String[] info = token.split(";");
    		
    		System.out.println(info.length);
    		for(String str:info){
    			System.out.println(str+"".contains(""));
    		}
		}
    	
	}
}
