package com.aurora.common.util;

import java.math.BigDecimal;
import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class BaseUtil {
	private final static String[] agent = { "Android", "iPhone", "iPod","iPad", "Windows Phone", "MQQBrowser" }; //定义移动端请求的所有可能类型
	/**
	* 判断User-Agent 是不是来自于手机
	* @param ua
	* @return
	*/
	public static boolean checkAgentIsMobile(String ua) {
		boolean flag = false;
		if (ua == null || "".equals(ua.trim())) {
			return flag;
		}
		if (!ua.contains("Windows NT") || (ua.contains("Windows NT") && ua.contains("compatible; MSIE 9.0;"))) {
			// 排除 苹果桌面系统
			if (!ua.contains("Windows NT") && !ua.contains("Macintosh")) {
				for (String item : agent) {
					if (ua.contains(item)) {
						flag = true;
						break;
					}
				}
			}
		}
		return flag;
	}

	/**
	 * 转换成(yyyy-MM-dd)
	 * 
	 * @param ds
	 * @return
	 * @throws Exception
	 */
	public static Date parseDate(String ds) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		if (ds.indexOf("/") > 0) {
			return sdf.parse(ds.replace("/", "-"));
		} else {
			return sdf.parse(ds);
		}
	}

	/**
	 * 转换成(yyyy-MM-dd)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToString(Date d) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(d);
	}

	/**
	 * 转换成(yyyyMMdd)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToStringNo(Date d) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(d);
	}

	/**
	 * 转换成(yyyyMMddHHmmss)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToStringNomm(Date d) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		return sdf.format(d);
	}

	/**
	 * 获取年份
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String year() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yy");
		return sdf.format(new Date());
	}
	
	/**
	 * 获取年份
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String year(Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		return sdf.format(date);
	}
	/**
	 * 获取月份
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String month(Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		return sdf.format(date);
	}
	
	/**根据日期，获取时间字符串
	 * @param date
	 * @return
	 */
	public static String getTime(Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(date);
	}
	/**
	 * 根据日期 找到对应天
	 */
	public static int getDay(Date date) throws Exception {
		SimpleDateFormat formatter = new SimpleDateFormat("dd");
		String str = formatter.format(date);
		return Integer.valueOf(str);
	}
	
	/**
	 * 根据年 月 获取对应的月份 天数
	 * */
	public static int getDaysByYearMonth(int year, int month) {
		
		Calendar a = Calendar.getInstance();
		a.set(Calendar.YEAR, year);
		a.set(Calendar.MONTH, month - 1);
		a.set(Calendar.DATE, 1);
		a.roll(Calendar.DATE, -1);
		int maxDate = a.get(Calendar.DATE);
		return maxDate;
	}
	
	/**
	 * 两个time之间的秒数<br>
	 * 例如：<br>
	 * 09:51:00和09:00:00之间的秒数
	 */
	public static long get2time(Time startTime, Time endTime) {
		return (endTime.getTime()-startTime.getTime())/1000;
	}
	
	/**
	 * Date转time<br>
	 * 例如：<br>
	 * 2017-06-01 09:51:00 转成 09:51:00
	 */
	public static Time parseDate2Time(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		String timeStr =  sdf.format(date);
		return Time.valueOf(timeStr);
	}

	/**
	 * 获取年月
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String yearAndMonth() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		return sdf.format(new Date());
	}

	/**
	 * 转换成(yyyy-MM-dd HH:mm:ss)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToStringss(Date d) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(d);
	}

	/**
	 * 转换成(yyyy-MM-dd HH:mm)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToStringTime(Date d) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return sdf.format(d);
	}

	/**
	 * long型的数据转换成(yyyy-MM-dd HH:mm:ss)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToStringss(Long l) throws Exception {
		Date date = new Date(l);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf.format(date);
	}

	/**
	 * 转换成(MM-dd HH:mm)
	 * 
	 * @param d
	 * @return
	 * @throws Exception
	 */
	public static String parseDateToStringDateTime(Date d) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
		return sdf.format(d);
	}

	/**
	 * 转换成(yyyy-MM-dd HH:mm)
	 * 
	 * @param ds
	 * @return
	 * @throws Exception
	 */
	public static Date parseDateTime(String ds) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		if (ds.indexOf("/") > 0) {
			return sdf.parse(ds.replace("/", "-"));
		} else {
			return sdf.parse(ds);
		}
	}

	/**
	 * 转换成(yyyy-MM-dd HH:mm:ss)
	 * 
	 * @param ds
	 * @return
	 * @throws Exception
	 */
	public static Date parseTimeStamp(String ds) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (ds.indexOf("/") > 0) {
			return sdf.parse(ds.replace("/", "-"));
		} else {
			return sdf.parse(ds);
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str) {
		if (str != null && !"".equals(str.trim())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true 大于零的
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(Long str) {
		if (str != null && str > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(Short str) {
		if (str != null && str > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true 大于零的
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(Double str) {
		if (str != null && str > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true 大于零的
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(Integer str) {
		if (str != null && str > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmptyDate(Date str) {
		if (str != null && !"".equals(str)) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmptyTime(Time str) {
		if (str != null && !"".equals(str)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmptySub(String[] str) {
		if (str != null && str.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmptySub(Integer[] str) {
		if (str != null && str.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmptySub(Short[] str) {
		if (str != null && str.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isEmptySub(Long[] str) {
		if (str != null && str.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 不为空返回true
	 * 
	 * @param str
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isEmptyList(List str) {
		if (str != null && str.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public static Double stringToDouble(String str) {
		if (str != null && !"".equals(str.trim())) {
			return Double.valueOf(str);
		} else {
			return Double.valueOf(0);
		}
	}

	public static Double stringToDouble(Object str) {
		if (str != null && !"".equals(str)) {
			return Double.valueOf((String) str);
		} else {
			return Double.valueOf(0);
		}
	}

	public final static Double exitAndDouble(Double d) {
		if (d != null && d > 0) {
			return d;
		} else {
			return Double.valueOf(0);
		}
	}

	public static Integer exitAndint(Integer d) {
		if (d != null && d > 0) {
			return d;
		} else {
			return Integer.valueOf(0);
		}
	}

	public static Date exitAndDate(Date d) {
		if (d != null && !"".equals(d)) {
			return d;
		} else {
			return new Date();
		}
	}

	/**
	 * 不区分大小写
	 * 
	 * @param str
	 * @return
	 */
	public static String convertString(String str) {
		String upStr = str.toUpperCase();
		String lowStr = str.toLowerCase();
		StringBuffer buf = new StringBuffer(str.length());
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == upStr.charAt(i)) {
				buf.append(lowStr.charAt(i));
			} else {
				buf.append(lowStr.charAt(i));
			}
		}
		return buf.toString();
	}

	/**
	 * 转换为大写
	 * 
	 * @param src
	 * @return
	 */
	public static String convertString1(String src) {
		char[] array = src.toCharArray();
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			temp = (int) array[i];
			if (temp <= 122 && temp >= 97) {
				array[i] = (char) (temp - 32);
			}
		}
		return String.valueOf(array);
	}

	/**
	 * double转换成String
	 * 
	 * @param str
	 * @return
	 */
	public static String doubleToString(Double str) {
		DecimalFormat df = new DecimalFormat();
		return df.format(str);
	}

	/**
	 * 数字转换成字母
	 * 
	 * @param i
	 * @return
	 */
	public static String getCha(Short i) {
		Character character = (char) (i + 64);
		return character.toString();
	}

	public static String getStringByDouble(Double d) {
		DecimalFormat df = new DecimalFormat("##0.00");
		return df.format(d);
	}

	private static final String[] beforeShuffle = new String[] { "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C",
			"D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X",
			"Y", "Z" };

	public static String random() {
		List<String> list = Arrays.asList(beforeShuffle);
		Collections.shuffle(list);
		StringBuilder sb = new StringBuilder();
		for (int i = 0, h = list.size(); i < h; i++) {
			sb.append(list.get(i));
		}
		String afterShuffle = sb.toString();
		String result = afterShuffle.substring(5, 9);
		return result;
	}

	/**
	 * 验证邮箱
	 * 
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 当前时间转换成Long(精确到秒)
	 * 
	 * @return
	 */
	public static long getNowDateToLong() {
		return System.currentTimeMillis() / 1000;
	}

	/**
	 * 大陆号码或香港号码均可
	 */
	public static boolean isPhoneLegal(String str) throws PatternSyntaxException {
		return isChinaPhoneLegal(str) || isHKPhoneLegal(str);
	}

	/**
	 * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数 此方法中前三位格式有： 13+任意数 15+除4的任意数 18+除1和4的任意数
	 * 17+除9的任意数 147
	 */
	public static boolean isChinaPhoneLegal(String str) throws PatternSyntaxException {
		String regExp = "^((13[0-9])|(15[^4])|(18[0-9])|(17[0-8])|(147))\\d{8}$";
		Pattern p = Pattern.compile(regExp);
		Matcher m = p.matcher(str);
		return m.matches();
	}

	/**
	 * 香港手机号码8位数，5|6|8|9开头+7位任意数
	 */
	public static boolean isHKPhoneLegal(String str) throws PatternSyntaxException {
		String regExp = "^(5|6|8|9)\\d{7}$";
		Pattern p = Pattern.compile(regExp);
		Matcher m = p.matcher(str);
		return m.matches();
	}

	/**
	 * <p>
	 * 获取去掉"-" UUID
	 * </p>
	 */
	public static synchronized String get32UUID() {
		return UUID.randomUUID().toString().replace("-", "");
	}

	/**
	 * 判断字符串是否是正整数
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 判断字符串日期格式 2016-01-01
	 * 
	 * @param date
	 * @return
	 */
	public static boolean isValidDate(String date) {
		Pattern pattern = Pattern.compile("(\\d){4}-(\\d){2}-(\\d){2}");
		Matcher matcher = pattern.matcher(date);
		return matcher.find();
	}

	private final static Pattern ATTR_PATTERN = Pattern.compile("<img[^<>]*?\\ssrc=['\"]?(.*?)['\"]?\\s.*?>",
			Pattern.CASE_INSENSITIVE);

	/**
	 * 获取文章里面的一个图片
	 * 
	 * @param s
	 * @return
	 */
	public static String getimg(String s) {
		Matcher matcher = ATTR_PATTERN.matcher(s);
		while (matcher.find()) {
			return matcher.group(1);
		}
		return null;
	}

	/**
	 * 提取文章里面的前50个文字
	 * 
	 * @param strHtml
	 * @return
	 */
	public static String StripHT(String strHtml) {
		String txtcontent = strHtml.replaceAll("</?[^>]+>", ""); // 剔出<html>的标签
		// txtcontent = txtcontent.replaceAll("<a>\\s*|\t|\r|\n</a>", "");//
		// 去除字符串中的空格,回车,换行符,制表符
		Pattern p = Pattern.compile("\\s*|\t|\r|\n");
		Matcher m = p.matcher(txtcontent);
		txtcontent = m.replaceAll("");
		if (txtcontent.length() > 50) {
			return txtcontent.substring(0, 50);
		}
		return txtcontent;
	}

	/**
	 * double类型比较大小
	 * 
	 * @param d1
	 * @param d2
	 * @return 大于0：d1大，等于0：相等，小于0：d2大
	 */
	public static int doubleCompare(Double d1, Double d2) {
		d1 = exitAndDouble(d1);
		d2 = exitAndDouble(d2);
		BigDecimal b1 = new BigDecimal(d1);
		BigDecimal b2 = new BigDecimal(d2);
		return b1.compareTo(b2);
	}

	/**
	 * 从001递增到999
	 * @param str
	 * @return
	 */
	public static String dizeng(String str) {
		int s = Integer.parseInt(str);
		s = ++s;
		s = s == 1000 ? 1 : s;
		String reslut = s >= 10 ? (s >= 100 ? s + "" : "0" + s) : "00" + s; // 计算// 转型
		return reslut;
	}

	public static void main(String[] args) throws Exception {
		/*
		 * String s =
		 * "<p>\r\n\t速度大撒旦\r\n</p>\r\n<p>\r\n\tsdasd捱三顶四a\r\n</p>\r\n<p>\r\n\ts发生的发生<img src=\"/aurora-oa/attachment/image/20170119/mmexport1483809658259.jpg\" alt=\"\" width=\"627\" height=\"628\" />\r\n</p>\r\n<p>\r\n\tss待发送\r\n</p>\r\n<p>\r\n\t恩恩二二\r\n</p>\r\n<p>\r\n\tdas但是\r\n</p>;"
		 * ;
		 * 
		 * Matcher matcher = ATTR_PATTERN.matcher(s); String str=""; while
		 * (matcher.find()) { str = matcher.group(1);
		 * System.out.println(matcher.group(1)); break; }
		 */

		// System.out.println(str=str.substring(1));
		// System.out.println(str.substring(1).substring(str.indexOf("/")));

		// String[] dd =str.split("/");
		// System.out.println(Arrays.deepToString(dd));
		//// System.out.println(str);

		// System.out.println(BaseUtil.StripHT(s));

		// System.out.println(parseDateToStringNo(new Date()));
		String s = "SCJHD123123123";
		System.out.println(s.substring(5));
		
		//25.001585,long 102.701426
		Double distance = BaseUtil.getDistanceByBaidu(102.701426, 25.001585, 102.71460114, 25.0491531);
		Double distance1 = BaseUtil.getDistanceByBaidu(102.701426, 25.001585, 102.711466, 25.010558);
		Double distance2 = BaseUtil.getDistanceByBaidu(102.701940, 25.001370, 102.702020, 25.001306);
		System.out.println(distance+","+distance1+"#"+distance2);

	}
	
	/**
	 * @description 判断传入的字段是否满足正则表达式
	 * @author hewei
	 * @title: chckRules
	 * @date 2017年3月27日 下午1:20:13
	 * @param regex
	 * @param str
	 * @return boolean
	 */
	public static boolean chckRulesByRegex(String regex,String str){
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(str);
		return m.matches();
	}
	
    /**
     * 百度测算距离
     * 通过经纬度获取距离(单位：米)   
     * @param lng1 经度 1
     * @param lat1 纬度 1 
     * @param lng2 经度 2 
     * @param lat2 纬度 2 
     * @return   
     */    
    public static double getDistanceByBaidu(double lng1, double lat1,double lng2, double lat2) {    
        double radLat1 = lat1*Math.PI/180.0;    
        double radLat2 = lat2*Math.PI/180.0;    
        double a = radLat1 - radLat2;    
        double b = lng1*Math.PI/180.0 - lng2*Math.PI/180.0;    
        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2)    
                + Math.cos(radLat1) * Math.cos(radLat2)    
                * Math.pow(Math.sin(b / 2), 2)));    
        s = s * 6378.137; // 地球半径： 6378.137;  
        s = Math.round(s * 10000d) / 10000d;    
        s = s*1000;    
        return s;    
    }
    
    /**
     * 高德测算距离
     * 通过经纬度获取距离(单位：米)   
     * @param lng1 经度 1
     * @param lat1 纬度 1 
     * @param lng2 经度 2 
     * @param lat2 纬度 2 
     * @return   
     */ 
	public static double getDistanceByGaode(double lng1, double latitude1, double lng2, double latitude2) {
		double lon1 = (Math.PI / 180) * lng1;
		double lon2 = (Math.PI / 180) * lng2;
		double lat1 = (Math.PI / 180) * latitude1;
		double lat2 = (Math.PI / 180) * latitude2;
		// 地球半径
		double R = 6371;
		// 两点间距离 km，如果想要米的话，结果*1000就可以了
		double d = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1))
				* R;
		return d * 1000;
	}
	/** 
     * 替换指定标签的属性和值 
     * @param str 需要处理的字符串 
     * @param tag 标签名称 
     * @param tagAttrib 要替换的标签属性值 
     * @param startTag 新标签开始标记 
     * @param endTag  新标签结束标记 
     * @return 
     * @author huweijun 
     * @date 2016年7月13日 下午7:15:32 
     */  
    public static String replaceHtmlTag(String str, String tag, String tagAttrib, String startTag, String endTag) {  
        String regxpForTag = "<\\s*" + tag + "\\s+([^>]*)\\s*" ;  
        String regxpForTagAttrib = tagAttrib + "=\\s*\"([^\"]+)\"" ;  
        Pattern patternForTag = Pattern.compile (regxpForTag,Pattern. CASE_INSENSITIVE );  
        Pattern patternForAttrib = Pattern.compile (regxpForTagAttrib,Pattern. CASE_INSENSITIVE );     
        Matcher matcherForTag = patternForTag.matcher(str);  
        StringBuffer sb = new StringBuffer();  
        boolean result = matcherForTag.find();  
        while (result) {  
            StringBuffer sbreplace = new StringBuffer( "<"+tag+" ");  
            Matcher matcherForAttrib = patternForAttrib.matcher(matcherForTag.group(1));  
            if (matcherForAttrib.find()) {  
                String attributeStr = matcherForAttrib.group(1);  
                matcherForAttrib.appendReplacement(sbreplace, startTag + attributeStr + endTag);  
            }  
            matcherForAttrib.appendTail(sbreplace);  
            matcherForTag.appendReplacement(sb, sbreplace.toString());  
            result = matcherForTag.find();  
        }  
        matcherForTag.appendTail(sb);           
        return sb.toString();  
    }
    /**
	 * 生成一位指定长度的随机数字
	 * @return
	 */
	public static String randomRandomCode(int codeLength) {
		Random rand = new Random();
		String result = "";
		for(int i=0; i<codeLength; i++) {
			result += rand.nextInt(10);
		}
		return result;
	}
	
	public static String smsCode(String code) {
		JSONObject jo = new JSONObject();
    	jo.put("code", code);
    	return JSON.toJSONString(jo);
	}
}
