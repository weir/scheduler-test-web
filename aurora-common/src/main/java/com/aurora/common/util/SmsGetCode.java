package com.aurora.common.util;

import java.io.IOException;
import java.util.Properties;

public class SmsGetCode {

	public static String readUrl(String key){
		Properties properties = new Properties();
		try {
			properties.load(SmsGetCode.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (String)properties.get(key);
	}
	
	public static void main(String[] args) {
		System.out.println(readUrl("smsCode"));
	}
}
