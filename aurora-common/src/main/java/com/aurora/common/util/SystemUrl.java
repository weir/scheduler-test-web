package com.aurora.common.util;

import java.io.IOException;
import java.util.Properties;

public class SystemUrl {
	private static Properties properties = new Properties();
	static{
		try {
			properties.load(SystemUrl.class.getClassLoader().getResourceAsStream("jdbc.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String readUrl(String key){
		return (String)properties.get(key);
	}
	
	public static Properties getProperties() {
		return properties;
	}
}
